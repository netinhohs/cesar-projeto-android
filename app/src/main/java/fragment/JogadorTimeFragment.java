package fragment;

import android.support.v4.app.Fragment;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;
import android.widget.TextView;

import com.example.netocarvalho.pernambucano.R;

import adapter.JogadorAdapter;
import model.Time;

/**
 * Created by Neto Carvalho on 08/01/2018.
 */

public class JogadorTimeFragment extends Fragment {

    private ListView mListJogadores;
    private JogadorAdapter mJogadorAdapter;
    private Time mTime;




    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        View root = inflater.inflate(R.layout.fragment_jogador, container, false);

        mListJogadores= root.findViewById(R.id.lvJogadores);

        mTime = (Time) getActivity().getIntent().getSerializableExtra(TimeFragment.CHAVE_TIME);

        mJogadorAdapter= new JogadorAdapter(getActivity(), mTime.getJogador());

        mListJogadores.setAdapter(mJogadorAdapter);

        return root;
    }
}
