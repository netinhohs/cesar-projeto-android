package fragment;

import android.app.Fragment;
import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.ProgressDialog;
import android.app.TaskStackBuilder;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.Snackbar;
import android.support.v4.app.NotificationCompat;
import android.support.v4.content.LocalBroadcastManager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.Toast;

import com.example.netocarvalho.pernambucano.DetalheTimeActivity;
import com.example.netocarvalho.pernambucano.R;
import com.example.netocarvalho.pernambucano.TimeActivity;
import com.google.gson.Gson;

import java.util.ArrayList;
import java.util.List;

import adapter.TimeAdapter;
import model.Brasileirao;
import model.Root;
import model.Time;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;

/**
 * Created by Neto Carvalho on 06/01/2018.
 */

public class TimeFragment extends Fragment {

    private static TimeFragment timeFragment;
    private ListView mListTimes;
    private List<Time> mTimes;
    private ProgressDialog mProgress;
    private TimeAdapter mTimeAdapter;

    private InternalBroadcast mReceiver;

    private NotificationManager mNotificationManager;

    public static final String BROADCAST_ACTION =
            "br.com.dts.broadcastreceiver.BROADCAST_ACTION_SECOND";
    public static final int SIMPLE_NOTIFICATION_ID = 10001;
    public static final String CHAVE_TIME = "time";

    public static TimeFragment newInstance() {
        if(timeFragment == null) {
            timeFragment = new TimeFragment();
        }

        return timeFragment;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        View root = inflater.inflate(R.layout.fragment_time, container, false);

        mListTimes = root.findViewById(R.id.lvTimes);
        mTimes = new ArrayList<>();
        new TimeTask().execute();

        init();

        sendNotifica();

        mReceiver = new InternalBroadcast();

        return root;

    }

    private void setupListView() {
        mTimeAdapter = new TimeAdapter(getActivity(), mTimes);

        mListTimes.setAdapter(mTimeAdapter);

        mListTimes.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                Time time = (Time) adapterView.getItemAtPosition(i);

                Intent it = new Intent(getActivity(), DetalheTimeActivity.class);
                it.putExtra(CHAVE_TIME, time);

                startActivity(it);
            }
        });
    }

    class TimeTask extends AsyncTask<Void, Void, Brasileirao> {


        @Override
        protected void onPreExecute() {
            mProgress = new ProgressDialog(getActivity());
            mProgress.setTitle(R.string.prog_title);

            mProgress.setMessage(getString(R.string.prog_details));
            mProgress.show();
        }


        @Override
        protected Brasileirao doInBackground(Void... voids) {
            OkHttpClient client = new OkHttpClient();

            Request request = new Request.Builder()
                    .url("https://dl.dropboxusercontent.com/s/1sspx1de3mngzqe/times.json")
                    .build();
            Response response = null;

            try {
                response = client.newCall(request).execute();
                String s = response.body().string();
                Gson gson = new Gson();
                Root brasileirao = gson.fromJson(s, Root.class);

                List<Time> timesTemp =  brasileirao.getBrasileirao().getTimes();

                mTimes.addAll(timesTemp);
            } catch (Throwable e) {
                e.printStackTrace();
                LocalBroadcastManager.getInstance(getActivity())
                        .sendBroadcast(new Intent(BROADCAST_ACTION));
            }
            return null;
        }

        @Override
        protected void onPostExecute(Brasileirao brasileirao) {

            if (mProgress != null && mProgress.isShowing()) {
                try {
                    mProgress.dismiss();
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
            setupListView();
        }
    }

    @Override
    public void onResume() {
        super.onResume();

        IntentFilter intentFilter = new IntentFilter(BROADCAST_ACTION);
        LocalBroadcastManager.getInstance(getActivity())
                .registerReceiver(mReceiver, intentFilter);
    }

    @Override
    public void onPause() {
        super.onPause();

        LocalBroadcastManager.getInstance(getActivity())
                .unregisterReceiver(mReceiver);
    }

    private void init(){
        mNotificationManager =
                (NotificationManager) getActivity().getSystemService(Context.NOTIFICATION_SERVICE);
    }

   private void sendNotifica(){
       NotificationCompat.Builder mBuilder =
               (NotificationCompat.Builder) new NotificationCompat.Builder(getContext())
                       .setSmallIcon(R.mipmap.ic_launcher)
                       .setContentTitle(getString(R.string.app_name))
                       .setContentText(getString(R.string.noti_details));


       Intent MyIntent = new Intent(Intent.ACTION_VIEW);
       PendingIntent StartIntent = PendingIntent.getActivity(getContext(),0,MyIntent, PendingIntent.FLAG_CANCEL_CURRENT);


       mNotificationManager.notify(SIMPLE_NOTIFICATION_ID, mBuilder.build());
   }

    private class InternalBroadcast extends BroadcastReceiver {
        @Override
        public void onReceive(Context context, Intent intent) {

            if (intent.getAction().equals(BROADCAST_ACTION)) {
                Toast.makeText(context, R.string.broad_send, Toast.LENGTH_LONG).show();
            }
        }
    }

}
