package fragment;

import android.net.Uri;
import android.support.v4.app.Fragment;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;
import android.content.Intent;

import com.example.netocarvalho.pernambucano.R;
import com.example.netocarvalho.pernambucano.TimeActivity;

import model.Time;

/**
 * Created by Neto Carvalho on 08/01/2018.
 */

public class DetalheTimeFragment extends Fragment {

    private TextView mTxtNomeTime;
    private TextView mTxtFundacaoTime;
    private TextView mTxtDescricaoTime;
    private TextView mTxtWikiTime;

    private Time mTime;

    public DetalheTimeFragment() {

    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

      //  mTime = (Time) getIntent().getSerializableExtra(TimeFragment.CHAVE_TIME);
      //  Toast.makeText(getContext(),"foi chamada",Toast.LENGTH_LONG);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {

        View root = inflater.inflate(R.layout.fragment_detalhe, container, false);

       mTime = (Time) getActivity().getIntent().getSerializableExtra(TimeFragment.CHAVE_TIME);


        mTxtNomeTime = (TextView) root.findViewById(R.id.txt_nome_time);
        mTxtFundacaoTime = (TextView) root.findViewById(R.id.txt_fundacao_time);
        mTxtDescricaoTime = (TextView) root.findViewById(R.id.txt_descricao_time);
        mTxtWikiTime = (TextView) root.findViewById(R.id.txt_wiki_time);



        if (mTime != null){
            mTxtWikiTime.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Intent intent = new Intent(Intent.ACTION_VIEW).setData(Uri.parse(mTime.getWiki()));
                    startActivity(intent);
                }
            });


            mTxtNomeTime.setText(mTime.getNome());
            mTxtFundacaoTime.setText(Integer.toString(mTime.getAno_fundacao()));
           mTxtDescricaoTime.setText(mTime.getDescricao());
        }
        return root;
       // return super.onCreateView(inflater, container, savedInstanceState);
    }
}
