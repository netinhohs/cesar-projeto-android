package com.example.netocarvalho.pernambucano;

import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.view.Menu;
import android.view.MenuItem;

import fragment.TimeFragment;

public class TimeActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_time);

        TimeFragment fragment = TimeFragment.newInstance();

        //Recupera a instancia do fragment manager responsável  por gerenciar os fragments.
        FragmentManager manager = getFragmentManager();
        //Inicia uma transação para adicionar, remover ou alterar o fragment.
        FragmentTransaction transaction = manager.beginTransaction();
        transaction.replace(R.id.fragment_time, fragment);
        //Realiza a alteração. Importante sem o commando abaixo o fragment não é atualizado.
        transaction.commit();

    }


}
