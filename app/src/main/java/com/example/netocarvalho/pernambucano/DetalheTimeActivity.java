package com.example.netocarvalho.pernambucano;

import android.support.v4.app.Fragment;
import android.support.design.widget.TabLayout;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import java.util.ArrayList;
import java.util.List;

import adapter.ViewPagerAdapter;
import fragment.DetalheTimeFragment;
import fragment.JogadorTimeFragment;

public class DetalheTimeActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detalhe_time);

        ViewPager viewPager = (ViewPager) findViewById(R.id.pager);
        ViewPagerAdapter adapter = new ViewPagerAdapter(getSupportFragmentManager());

        String detalhe = getString(R.string.tab_details);
        String jogador = getString(R.string.tab_player);

        // Add Fragments to adapter one by one
        adapter.addFragment(new DetalheTimeFragment(), detalhe);
        adapter.addFragment(new JogadorTimeFragment(), jogador);
        viewPager.setAdapter(adapter);

        TabLayout tabLayout = (TabLayout) findViewById(R.id.tabs);
        tabLayout.setupWithViewPager(viewPager);
    }



}
