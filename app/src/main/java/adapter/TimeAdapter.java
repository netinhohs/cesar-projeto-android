package adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.netocarvalho.pernambucano.R;
import com.squareup.picasso.Picasso;

import java.util.List;

import model.Time;

/**
 * Created by Neto Carvalho on 06/01/2018.
 */

public class TimeAdapter extends BaseAdapter {
    private Context mContext;
    private List<Time> mTimes;

    public TimeAdapter(Context mContext, List<Time> mTimes) {
        this.mContext = mContext;
        this.mTimes = mTimes;
    }

    @Override
    public int getCount() {
        return (mTimes != null) ? mTimes.size() : 0;
    }

    @Override
    public Object getItem(int i) {
        return mTimes.get(i);
    }

    @Override
    public long getItemId(int i) {
        return 0;
    }

    @Override
    public View getView(int i, View view, ViewGroup viewGroup) {
        Time time = mTimes.get(i);

        ViewHolder holder = null;

        if (view == null) {
            view = LayoutInflater.from(mContext).inflate(R.layout.item_time, null);

            holder = new ViewHolder();
            holder.imgTime = (ImageView) view.findViewById(R.id.imgTime);
            holder.txtTimeNome = (TextView) view.findViewById(R.id.txtTimeNome);

            view.setTag(holder);

        } else {
            holder = (ViewHolder)view.getTag();
        }

        Picasso.with(mContext)
                .load(time.getImg())
                .into(holder.imgTime);
        holder.txtTimeNome.setText(time.getNome());

        return view;
    }

    static class ViewHolder {
        ImageView imgTime;
        TextView txtTimeNome;
    }
}
