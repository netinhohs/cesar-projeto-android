package adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.netocarvalho.pernambucano.R;
import com.squareup.picasso.Picasso;

import java.util.List;

import model.Jogador;

/**
 * Created by Neto Carvalho on 11/01/2018.
 */

public class JogadorAdapter extends BaseAdapter {

    private Context mContext;
    private List<Jogador> mJogadores;

    public JogadorAdapter(Context mContext, List<Jogador> mJogadores) {
        this.mContext = mContext;
        this.mJogadores = mJogadores;
    }

    @Override
    public int getCount() { return (mJogadores != null) ? mJogadores.size() : 0; }

    @Override
    public Object getItem(int i) {
        return mJogadores.get(i);
    }

    @Override
    public long getItemId(int i) {
        return 0;
    }

    @Override
    public View getView(int i, View view, ViewGroup viewGroup) {
        Jogador jogador = mJogadores.get(i);

        ViewHolder holder = null;

        if (view == null) {
            view = LayoutInflater.from(mContext).inflate(R.layout.item_jogador,null);

            holder = new ViewHolder();

            holder.imgJogador = (ImageView) view.findViewById(R.id.imgJogador);
            holder.txtNomeJogador = (TextView) view.findViewById(R.id.txtNomeJogador);

            view.setTag(holder);

        } else {
            holder = (ViewHolder)view.getTag();
        }

        if (!jogador.getImg().isEmpty()){
            Picasso.with(mContext)
                    .load(jogador.getImg())
                    .into(holder.imgJogador);
        }
        holder.txtNomeJogador.setText(jogador.getNome());

        return view;
    }

    static class ViewHolder {
        ImageView imgJogador;
        TextView txtNomeJogador;
    }
}
