package broadcast;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.widget.Toast;

import fragment.TimeFragment;

/**
 * Created by Neto Carvalho on 13/01/2018.
 */

public class BroadCast extends BroadcastReceiver {
    @Override
    public void onReceive(Context context, Intent it) {

        String action = it.getAction();
        Toast.makeText(context, "Ação: "+ action, Toast.LENGTH_LONG).show();

        if (action.equals(Intent.ACTION_BOOT_COMPLETED)) {
            Intent intent = new Intent(context, TimeFragment.class);
            //start a new task process
            intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            context.startActivity(intent);
        }

    }
}
