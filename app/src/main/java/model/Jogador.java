package model;

import java.io.Serializable;

/**
 * Created by Neto Carvalho on 06/01/2018.
 */

public class Jogador implements Serializable {

    private String nome;
    private String img;

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public String getImg() {
        return img;
    }

    public void setImg(String img) {
        this.img = img;
    }
}
