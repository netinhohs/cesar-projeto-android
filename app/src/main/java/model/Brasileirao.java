package model;

import java.io.Serializable;
import java.util.List;

/**
 * Created by Neto Carvalho on 06/01/2018.
 */

public class Brasileirao implements Serializable {

    private List<Time> time;

    public List<Time> getTimes() {
        return time;
    }

    public void setTimes(List<Time> time) {
        this.time = time;
    }
}
