package model;

import java.io.Serializable;

/**
 * Created by Neto Carvalho on 06/01/2018.
 */

public class Root implements Serializable {

    private Brasileirao brasileirao;

    public Brasileirao getBrasileirao() {
        return brasileirao;
    }

    public void setBrasileirao(Brasileirao brasileirao) {
        this.brasileirao = brasileirao;
    }
}
